﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace pruebaVisionBackend.Migrations
{
    public partial class modificacion_tabla_factura : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "fechaFactura",
                table: "facturaEntities",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "fechaFactura",
                table: "facturaEntities");
        }
    }
}
