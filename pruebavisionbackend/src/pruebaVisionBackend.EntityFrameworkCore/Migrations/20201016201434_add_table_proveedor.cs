﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace pruebaVisionBackend.Migrations
{
    public partial class add_table_proveedor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_facturaEntities_clientesEntities_IdCliente",
                table: "facturaEntities");

            migrationBuilder.DropIndex(
                name: "IX_facturaEntities_IdCliente",
                table: "facturaEntities");

            migrationBuilder.DropColumn(
                name: "IdCliente",
                table: "facturaEntities");

            migrationBuilder.AddColumn<int>(
                name: "IdProveedor",
                table: "facturaEntities",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ProveedoresEntities",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    CreatorUserId = table.Column<long>(nullable: true),
                    LastModificationTime = table.Column<DateTime>(nullable: true),
                    LastModifierUserId = table.Column<long>(nullable: true),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DeleterUserId = table.Column<long>(nullable: true),
                    DeletionTime = table.Column<DateTime>(nullable: true),
                    nombre = table.Column<string>(nullable: true),
                    identificacion = table.Column<string>(nullable: true),
                    tipoIdentificacion = table.Column<string>(nullable: true),
                    direccion = table.Column<string>(nullable: true),
                    telefono = table.Column<string>(nullable: true),
                    regimen = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProveedoresEntities", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_facturaEntities_IdProveedor",
                table: "facturaEntities",
                column: "IdProveedor");

            migrationBuilder.AddForeignKey(
                name: "FK_facturaEntities_ProveedoresEntities_IdProveedor",
                table: "facturaEntities",
                column: "IdProveedor",
                principalTable: "ProveedoresEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_facturaEntities_ProveedoresEntities_IdProveedor",
                table: "facturaEntities");

            migrationBuilder.DropTable(
                name: "ProveedoresEntities");

            migrationBuilder.DropIndex(
                name: "IX_facturaEntities_IdProveedor",
                table: "facturaEntities");

            migrationBuilder.DropColumn(
                name: "IdProveedor",
                table: "facturaEntities");

            migrationBuilder.AddColumn<int>(
                name: "IdCliente",
                table: "facturaEntities",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_facturaEntities_IdCliente",
                table: "facturaEntities",
                column: "IdCliente");

            migrationBuilder.AddForeignKey(
                name: "FK_facturaEntities_clientesEntities_IdCliente",
                table: "facturaEntities",
                column: "IdCliente",
                principalTable: "clientesEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
