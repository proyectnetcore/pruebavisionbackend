﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using pruebaVisionBackend.Configuration;
using pruebaVisionBackend.Web;

namespace pruebaVisionBackend.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class pruebaVisionBackendDbContextFactory : IDesignTimeDbContextFactory<pruebaVisionBackendDbContext>
    {
        public pruebaVisionBackendDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<pruebaVisionBackendDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            pruebaVisionBackendDbContextConfigurer.Configure(builder, configuration.GetConnectionString(pruebaVisionBackendConsts.ConnectionStringName));

            return new pruebaVisionBackendDbContext(builder.Options);
        }
    }
}
