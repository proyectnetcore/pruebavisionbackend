﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using pruebaVisionBackend.Authorization.Roles;
using pruebaVisionBackend.Authorization.Users;
using pruebaVisionBackend.MultiTenancy;
using pruebaVisionBackend.Entidades;

namespace pruebaVisionBackend.EntityFrameworkCore
{
    public class pruebaVisionBackendDbContext : AbpZeroDbContext<Tenant, Role, User, pruebaVisionBackendDbContext>
    {
        /* Define a DbSet for each entity of the application */
        
        public pruebaVisionBackendDbContext(DbContextOptions<pruebaVisionBackendDbContext> options)
            : base(options)
        {
        }

        public DbSet<ClientesEntity> clientesEntities { get; set; }
        public DbSet<FacturaEntity> facturaEntities { get; set; }
        public DbSet<DetalleFacturaEntity> detalleFacturaEntities { get; set; }
        public DbSet<ProveedoresEntity> ProveedoresEntities { get; set; }
    }
}
