using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace pruebaVisionBackend.EntityFrameworkCore
{
    public static class pruebaVisionBackendDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<pruebaVisionBackendDbContext> builder, string connectionString)
        {
            builder.UseNpgsql(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<pruebaVisionBackendDbContext> builder, DbConnection connection)
        {
            builder.UseNpgsql(connection);
        }
    }
}
