﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using pruebaVisionBackend.Controllers;
using pruebaVisionBackend.Facturas;
using pruebaVisionBackend.Facturas.Dto;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace pruebaVisionBackend.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FacturaController : pruebaVisionBackendControllerBase
    {
        // GET: api/<FacturaController>
        [AbpAuthorize]
        [HttpGet]
        public async Task<List<FacturasDto>> Get([FromServices] IFacturasAppService service)
        {
            return await service.FindFacturas();
        }

        // GET api/<FacturaController>/5
        [AbpAuthorize]
        [HttpGet("{identificacion}")]
        public async Task<FacturasDto> Get([FromServices] IFacturasAppService service,string identificacion)
        {
            return await service.FindForNumeroFactura(identificacion);
        }

        [AbpAuthorize]
        [HttpGet("reporte/{fecha}")]
        public async Task<List<FacturasOutputDto>> reporte([FromServices] IFacturasAppService service, string fecha)
        {
            return await service.ReporForDay(fecha);
        }

        // POST api/<FacturaController>
        [AbpAuthorize]
        [HttpPost]
        public async Task<Boolean> Post([FromServices] IFacturasAppService service,[FromForm] FacturasDto input)
        {
            return await service.RegistraActualiza(input);
        }

        // PUT api/<FacturaController>/5
        [AbpAuthorize]
        [HttpPut]
        public async Task<Boolean> Put([FromServices] IFacturasAppService service, [FromForm] FacturasDto input)
        {
            return await service.RegistraActualiza(input);
        }

        // DELETE api/<FacturaController>/5
        [AbpAuthorize]
        [HttpDelete("{id}")]
        public async Task<Boolean> Delete([FromServices] IFacturasAppService service,int id)
        {
            return await service.EliminaFactura(id);
        }
    }
}
