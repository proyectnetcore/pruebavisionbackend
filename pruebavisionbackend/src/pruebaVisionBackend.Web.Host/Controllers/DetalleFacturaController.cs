﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using pruebaVisionBackend.Controllers;
using pruebaVisionBackend.DetalleFactura;
using pruebaVisionBackend.DetalleFactura.Dto;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace pruebaVisionBackend.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DetalleFacturaController : pruebaVisionBackendControllerBase
    {
        // GET: api/<DetalleFacturaController>
        [AbpAuthorize]
        [HttpGet]
        public async Task<List<DetalleDto>> Get([FromServices] IDetalleFacturaAppService service)
        {
            return await service.FindDetalles();
        }

        // GET api/<DetalleFacturaController>/5
        [AbpAuthorize]
        [HttpGet("{factura}")]
        public async Task<List<DetalleDto>> Get([FromServices] IDetalleFacturaAppService service,int factura)
        {
            return await service.FindForNumeroFactura(factura);
        }

        // POST api/<DetalleFacturaController>
        [AbpAuthorize]
        [HttpPost]
        public async Task<Boolean> Post([FromServices] IDetalleFacturaAppService service,[FromForm] DetalleDto input)
        {
            return await service.RegistraActualiza(input);
        }

        // PUT api/<DetalleFacturaController>/5
        [AbpAuthorize]
        [HttpPut]
        public async Task<Boolean> Put([FromServices] IDetalleFacturaAppService service, [FromForm] DetalleDto input)
        {
            return await service.RegistraActualiza(input);
        }

        // DELETE api/<DetalleFacturaController>/5
        [AbpAuthorize]
        [HttpDelete("{id}")]
        public async Task<Boolean> Delete([FromServices] IDetalleFacturaAppService service,int id)
        {
            return await service.EliminaDetalle(id);
        }
    }
}
