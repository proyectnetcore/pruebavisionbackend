﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Authorization;
using Microsoft.AspNetCore.Mvc;
using pruebaVisionBackend.Controllers;
using pruebaVisionBackend.Proveedores;
using pruebaVisionBackend.Proveedores.Dto;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace pruebaVisionBackend.Web.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProveedoresController : pruebaVisionBackendControllerBase
    {
        // GET: api/<ProveedoresController>
        [AbpAuthorize]
        [HttpGet]
        public async Task<List<ProveedorDto>> Get([FromServices] IProveedoresAppService service)
        {
            return await service.FindProveedor();
        }

        // GET api/<ProveedoresController>/5
        [AbpAuthorize]
        [HttpGet("{identificacion}")]
        public async Task<ProveedorDto> Get([FromServices] IProveedoresAppService service, string identificacion)
        {
            return await service.FindForIndentificacion(identificacion);
        }

        // POST api/<ProveedoresController>
        [AbpAuthorize]
        [HttpPost]
        public async Task<Boolean> Post([FromServices] IProveedoresAppService service, [FromForm] ProveedorDto input)
        {
            return await service.RegistraActualiza(input);
        }

        // PUT api/<ProveedoresController>/5
        [AbpAuthorize]
        [HttpPut]
        public async Task<Boolean> Put([FromServices] IProveedoresAppService service, [FromForm] ProveedorDto input)
        {
            return await service.RegistraActualiza(input);
        }

        // DELETE api/<ProveedoresController>/5
        [AbpAuthorize]
        [HttpDelete("{id}")]
        public async Task<Boolean> Delete([FromServices] IProveedoresAppService service, int id)
        {
            return await service.EliminaProveedor(id);
        }
    }
}
