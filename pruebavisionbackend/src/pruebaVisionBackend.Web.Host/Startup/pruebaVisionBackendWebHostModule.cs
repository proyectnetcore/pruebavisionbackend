﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using pruebaVisionBackend.Configuration;

namespace pruebaVisionBackend.Web.Host.Startup
{
    [DependsOn(
       typeof(pruebaVisionBackendWebCoreModule))]
    public class pruebaVisionBackendWebHostModule: AbpModule
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public pruebaVisionBackendWebHostModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(pruebaVisionBackendWebHostModule).GetAssembly());
        }
    }
}
