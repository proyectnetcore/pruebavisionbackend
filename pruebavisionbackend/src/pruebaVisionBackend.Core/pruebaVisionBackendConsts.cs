﻿namespace pruebaVisionBackend
{
    public class pruebaVisionBackendConsts
    {
        public const string LocalizationSourceName = "pruebaVisionBackend";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
