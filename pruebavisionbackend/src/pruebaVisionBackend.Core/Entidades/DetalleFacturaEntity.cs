﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace pruebaVisionBackend.Entidades
{
    public class DetalleFacturaEntity: FullAuditedEntity
    {
        [ForeignKey("IdFactura")]
        public FacturaEntity factura { get; set; }
        public int IdFactura { get; set; }
        public string producto { get; set; }
        public double cantidad { get; set; }
        public double valorUnidad { get; set; }
        public double total { get; set; }
    }
}
