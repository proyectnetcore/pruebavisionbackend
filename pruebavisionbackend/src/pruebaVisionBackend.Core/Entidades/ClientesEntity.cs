﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;

namespace pruebaVisionBackend.Entidades
{
    public class ClientesEntity: FullAuditedEntity
    {
        public string nombre { get; set; }
        public string identificacion { get; set; }
        public string tipoIdentificacion { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
    }
}
