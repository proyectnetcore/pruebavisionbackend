﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Globalization;
using System.Text;

namespace pruebaVisionBackend.Entidades
{
    public class FacturaEntity: FullAuditedEntity
    {
        public string facturaVenta { get; set; }
        [ForeignKey("IdProveedor")]
        public ProveedoresEntity proveedoresEntity { get; set; }
        public int IdProveedor { get; set; }
        public string fechaFactura { get; set; }
        public string estado { get; set; }
        public double iva { get; set; }
        public double total { get; set; }
    }
}
