﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace pruebaVisionBackend.Localization
{
    public static class pruebaVisionBackendLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(pruebaVisionBackendConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(pruebaVisionBackendLocalizationConfigurer).GetAssembly(),
                        "pruebaVisionBackend.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
