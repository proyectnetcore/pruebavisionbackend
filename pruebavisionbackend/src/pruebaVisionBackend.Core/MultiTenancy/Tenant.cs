﻿using Abp.MultiTenancy;
using pruebaVisionBackend.Authorization.Users;

namespace pruebaVisionBackend.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
