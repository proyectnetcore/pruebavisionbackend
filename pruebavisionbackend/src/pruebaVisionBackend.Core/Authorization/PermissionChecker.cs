﻿using Abp.Authorization;
using pruebaVisionBackend.Authorization.Roles;
using pruebaVisionBackend.Authorization.Users;

namespace pruebaVisionBackend.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
