﻿using Abp.AutoMapper;
using pruebaVisionBackend.Authentication.External;

namespace pruebaVisionBackend.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
