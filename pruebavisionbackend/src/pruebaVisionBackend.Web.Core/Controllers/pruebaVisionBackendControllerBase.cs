using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace pruebaVisionBackend.Controllers
{
    public abstract class pruebaVisionBackendControllerBase: AbpController
    {
        protected pruebaVisionBackendControllerBase()
        {
            LocalizationSourceName = pruebaVisionBackendConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
