﻿using AutoMapper;
using pruebaVisionBackend.Entidades;
using pruebaVisionBackend.Facturas.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace pruebaVisionBackend.AutoMapProfile
{
    public class FacturaProfile: Profile
    {
        public FacturaProfile()
        {
            CreateMap<FacturaEntity, FacturasDto>();
            CreateMap<FacturasDto, FacturaEntity>();
            CreateMap<FacturaEntity, FacturasOutputDto>();
            CreateMap<FacturasOutputDto, FacturaEntity>();
        }
    }
}
