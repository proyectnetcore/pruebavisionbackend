﻿using AutoMapper;
using pruebaVisionBackend.Entidades;
using pruebaVisionBackend.Proveedores.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace pruebaVisionBackend.AutoMapProfile
{
    public class ProveedorProfile: Profile
    {
        public ProveedorProfile()
        {
            CreateMap<ProveedoresEntity, ProveedorDto>();
            CreateMap<ProveedorDto, ProveedoresEntity>();

        }
    }
}
