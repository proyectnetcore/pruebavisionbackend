﻿using AutoMapper;
using pruebaVisionBackend.DetalleFactura.Dto;
using pruebaVisionBackend.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace pruebaVisionBackend.AutoMapProfile
{
    public class DetalleFacturaProfile: Profile
    {
        public DetalleFacturaProfile()
        {
            CreateMap<DetalleFacturaEntity, DetalleDto>();
            CreateMap<DetalleDto, DetalleFacturaEntity>();

        }
    }
}
