﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using pruebaVisionBackend.Authorization;

namespace pruebaVisionBackend
{
    [DependsOn(
        typeof(pruebaVisionBackendCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class pruebaVisionBackendApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<pruebaVisionBackendAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(pruebaVisionBackendApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}
