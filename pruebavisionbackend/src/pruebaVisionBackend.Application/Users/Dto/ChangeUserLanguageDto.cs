using System.ComponentModel.DataAnnotations;

namespace pruebaVisionBackend.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}