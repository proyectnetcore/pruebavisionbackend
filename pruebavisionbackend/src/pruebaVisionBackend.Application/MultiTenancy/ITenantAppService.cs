﻿using Abp.Application.Services;
using pruebaVisionBackend.MultiTenancy.Dto;

namespace pruebaVisionBackend.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

