﻿namespace pruebaVisionBackend.Authorization.Accounts.Dto
{
    public enum TenantAvailabilityState
    {
        Available = 1,
        InActive,
        NotFound
    }
}
