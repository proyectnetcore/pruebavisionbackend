﻿using System.Threading.Tasks;
using Abp.Application.Services;
using pruebaVisionBackend.Authorization.Accounts.Dto;

namespace pruebaVisionBackend.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
