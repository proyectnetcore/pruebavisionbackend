﻿using Abp.Application.Services.Dto;

namespace pruebaVisionBackend.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

