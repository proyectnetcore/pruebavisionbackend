﻿using System.Threading.Tasks;
using Abp.Application.Services;
using pruebaVisionBackend.Sessions.Dto;

namespace pruebaVisionBackend.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
