﻿using System.Threading.Tasks;
using pruebaVisionBackend.Configuration.Dto;

namespace pruebaVisionBackend.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
