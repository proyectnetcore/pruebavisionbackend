﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using pruebaVisionBackend.Configuration.Dto;

namespace pruebaVisionBackend.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : pruebaVisionBackendAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
