﻿using pruebaVisionBackend.DetalleFactura.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace pruebaVisionBackend.DetalleFactura
{
    public interface IDetalleFacturaAppService
    {
        Task<Boolean> RegistraActualiza(DetalleDto input);
        Task<Boolean> EliminaDetalle(int id);
        Task<List<DetalleDto>> FindDetalles();
        Task<List<DetalleDto>> FindForNumeroFactura(int factura);
    }
}
