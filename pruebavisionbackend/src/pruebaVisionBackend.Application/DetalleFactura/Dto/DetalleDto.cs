﻿using pruebaVisionBackend.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace pruebaVisionBackend.DetalleFactura.Dto
{
    public class DetalleDto
    {
        public int id { get; set; }
        public int IdFactura { get; set; }
        public string producto { get; set; }
        public double cantidad { get; set; }
        public double valorUnidad { get; set; }
        public double total { get; set; }
    }
}
