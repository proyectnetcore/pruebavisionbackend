﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using pruebaVisionBackend.DetalleFactura.Dto;
using pruebaVisionBackend.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pruebaVisionBackend.DetalleFactura
{
    public class DetalleFacturaAppService : pruebaVisionBackendAppServiceBase, IDetalleFacturaAppService
    {
        private readonly IRepository<DetalleFacturaEntity> _detalleRepository;
        public DetalleFacturaAppService(IRepository<DetalleFacturaEntity> detalleRepository)
        {
            _detalleRepository = detalleRepository;
        }

        public async Task<bool> EliminaDetalle(int id)
        {
            try
            {
                await _detalleRepository.DeleteAsync(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<DetalleDto>> FindDetalles()
        {
            try
            {
                var entity = await _detalleRepository.GetAllListAsync();
                return ObjectMapper.Map<List<DetalleDto>>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<DetalleDto>> FindForNumeroFactura(int factura)
        {
            try
            {
                var entity = await _detalleRepository.GetAll().Where(x => x.IdFactura == factura).ToListAsync();
                var resp= ObjectMapper.Map<List<DetalleDto>>(entity);
                return resp;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> RegistraActualiza(DetalleDto input)
        {
            try
            {
                bool respuesta = false;
                var entity = ObjectMapper.Map<DetalleFacturaEntity>(input);
                var resultado = await _detalleRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (resultado > 0) respuesta = true;
                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
