﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pruebaVisionBackend.Proveedores.Dto
{
    public class ProveedorDto
    {
        public int Id { get; set; }
        public string nombre { get; set; }
        public string identificacion { get; set; }
        public string tipoIdentificacion { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public string regimen { get; set; }
    }
}
