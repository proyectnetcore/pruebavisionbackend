﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using pruebaVisionBackend.Entidades;
using pruebaVisionBackend.Proveedores.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pruebaVisionBackend.Proveedores
{
    public class ProveedoresAppService : pruebaVisionBackendAppServiceBase, IProveedoresAppService
    {
        private readonly IRepository<ProveedoresEntity> _proveedorRepository;
        public ProveedoresAppService(IRepository<ProveedoresEntity> proveedorRepository)
        {
            _proveedorRepository = proveedorRepository;
        }
        public async Task<bool> EliminaProveedor(int id)
        {
            try
            {
                await _proveedorRepository.DeleteAsync(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<ProveedorDto> FindForIndentificacion(string identificacion)
        {
            try
            {
                var entity = await _proveedorRepository.GetAll().Where(x=>x.identificacion==identificacion).FirstOrDefaultAsync();
                return ObjectMapper.Map<ProveedorDto>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<ProveedorDto>> FindProveedor()
        {
            try
            {
                var entity = await _proveedorRepository.GetAllListAsync();
                return ObjectMapper.Map<List<ProveedorDto>>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> RegistraActualiza(ProveedorDto input)
        {
            try
            {
                bool respuesta = false;
                var entity = ObjectMapper.Map<ProveedoresEntity>(input);
                var resultado = await _proveedorRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (resultado > 0) respuesta = true;
                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
