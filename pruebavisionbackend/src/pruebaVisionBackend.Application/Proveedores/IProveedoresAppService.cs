﻿using pruebaVisionBackend.Proveedores.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace pruebaVisionBackend.Proveedores
{
    public interface IProveedoresAppService
    {
        Task<Boolean> RegistraActualiza(ProveedorDto input);
        Task<Boolean> EliminaProveedor(int id);
        Task<List<ProveedorDto>> FindProveedor();
        Task<ProveedorDto> FindForIndentificacion(string identificacion);
    }
}
