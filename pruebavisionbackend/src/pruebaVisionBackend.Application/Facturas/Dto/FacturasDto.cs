﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pruebaVisionBackend.Facturas.Dto
{
    public class FacturasDto
    {
        public int Id { get; set; }
        public string facturaVenta { get; set; }
        public string fechaFactura { get; set; }
        public int IdProveedor { get; set; }
        public string estado { get; set; }
        public double iva { get; set; }
        public double total { get; set; }
    }
}
