﻿using pruebaVisionBackend.Facturas.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace pruebaVisionBackend.Facturas
{
    public interface IFacturasAppService
    {
        Task<Boolean> RegistraActualiza(FacturasDto input);
        Task<Boolean> EliminaFactura(int id);
        Task<List<FacturasDto>> FindFacturas();
        Task<FacturasDto> FindForNumeroFactura(string identificacion);
        Task<List<FacturasOutputDto>> ReporForDay(string fecha);
    }
}
