﻿using Abp.Domain.Repositories;
using Abp.UI;
using Microsoft.EntityFrameworkCore;
using pruebaVisionBackend.Entidades;
using pruebaVisionBackend.Facturas.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pruebaVisionBackend.Facturas
{
    public class FacturasAppService : pruebaVisionBackendAppServiceBase, IFacturasAppService
    {
        private readonly IRepository<FacturaEntity> _facturarRepository;
        public FacturasAppService(IRepository<FacturaEntity> facturarRepository)
        {
            _facturarRepository = facturarRepository;
        }
        public async Task<bool> EliminaFactura(int id)
        {
            try
            {
                await _facturarRepository.DeleteAsync(id);
                return true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<FacturasDto>> FindFacturas()
        {
            try
            {
                var entity = await _facturarRepository.GetAllListAsync();
                return ObjectMapper.Map<List<FacturasDto>>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<FacturasDto> FindForNumeroFactura(string identificacion)
        {
            try
            {
                var entity = await _facturarRepository.GetAll().Where(x => x.facturaVenta == identificacion).FirstOrDefaultAsync();
                return ObjectMapper.Map<FacturasDto>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<bool> RegistraActualiza(FacturasDto input)
        {
            try
            {
                bool respuesta = false;
                var entity = ObjectMapper.Map<FacturaEntity>(input);
                var resultado = await _facturarRepository.InsertOrUpdateAndGetIdAsync(entity);
                if (resultado > 0) respuesta = true;
                return respuesta;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }

        public async Task<List<FacturasOutputDto>> ReporForDay(string fecha)
        {
            try
            {
                var entity = await _facturarRepository.GetAll().Where(x => x.fechaFactura.Contains(fecha) )
                    .Include(x=>x.proveedoresEntity)
                    .ToListAsync();
                return ObjectMapper.Map<List<FacturasOutputDto>>(entity);
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException(ex.Message);
            }
        }
    }
}
