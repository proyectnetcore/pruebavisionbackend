using Microsoft.Extensions.Configuration;
using Castle.MicroKernel.Registration;
using Abp.Events.Bus;
using Abp.Modules;
using Abp.Reflection.Extensions;
using pruebaVisionBackend.Configuration;
using pruebaVisionBackend.EntityFrameworkCore;
using pruebaVisionBackend.Migrator.DependencyInjection;

namespace pruebaVisionBackend.Migrator
{
    [DependsOn(typeof(pruebaVisionBackendEntityFrameworkModule))]
    public class pruebaVisionBackendMigratorModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public pruebaVisionBackendMigratorModule(pruebaVisionBackendEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbSeed = true;

            _appConfiguration = AppConfigurations.Get(
                typeof(pruebaVisionBackendMigratorModule).GetAssembly().GetDirectoryPathOrNull()
            );
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                pruebaVisionBackendConsts.ConnectionStringName
            );

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
            Configuration.ReplaceService(
                typeof(IEventBus), 
                () => IocManager.IocContainer.Register(
                    Component.For<IEventBus>().Instance(NullEventBus.Instance)
                )
            );
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(pruebaVisionBackendMigratorModule).GetAssembly());
            ServiceCollectionRegistrar.Register(IocManager);
        }
    }
}
