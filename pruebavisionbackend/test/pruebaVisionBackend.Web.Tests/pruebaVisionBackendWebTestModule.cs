﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using pruebaVisionBackend.EntityFrameworkCore;
using pruebaVisionBackend.Web.Startup;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace pruebaVisionBackend.Web.Tests
{
    [DependsOn(
        typeof(pruebaVisionBackendWebMvcModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class pruebaVisionBackendWebTestModule : AbpModule
    {
        public pruebaVisionBackendWebTestModule(pruebaVisionBackendEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(pruebaVisionBackendWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(pruebaVisionBackendWebMvcModule).Assembly);
        }
    }
}