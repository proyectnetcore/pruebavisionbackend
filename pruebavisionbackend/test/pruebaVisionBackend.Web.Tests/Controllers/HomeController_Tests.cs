﻿using System.Threading.Tasks;
using pruebaVisionBackend.Models.TokenAuth;
using pruebaVisionBackend.Web.Controllers;
using Shouldly;
using Xunit;

namespace pruebaVisionBackend.Web.Tests.Controllers
{
    public class HomeController_Tests: pruebaVisionBackendWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            await AuthenticateAsync(null, new AuthenticateModel
            {
                UserNameOrEmailAddress = "admin",
                Password = "123qwe"
            });

            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>(nameof(HomeController.Index))
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}